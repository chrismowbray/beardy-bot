# beardy-bot

![build status](https://gitlab.com/chrismowbray/beardy-bot/badges/master/build.svg)


### Auto update your twitter bio with the number of days you have been growing your manly face bush

1.  Generate [Twitter API](https://developer.twitter.com/en/apps/create) keys/secrets for the account you want to post to
2.  Update config.js with the keys from the above step. Use environment variables for added security if deploying.
    ```
    module.exports = {
        consumer_key: "your_twitter_api_key",
        consumer_secret: "your_twitter_api_secret_key",
        access_token_key: "your_twitter_access_token",
        access_token_secret: "your_twitter_access_token_secret"
    };
    ```
3. Run app using `npm install` then `npm start`
4. Enjoy your beardy bio updates!

   ![beardy bio](beardy_bio.PNG)