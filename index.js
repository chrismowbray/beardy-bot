const Twitter = require("twitter");
const config = require("./config.js");

const client = new Twitter(config);

/*
 * Get current twitter bio. Needs to `days to increment` surrounded in *
 * e.g. "6 days since our last nonsense" - https://bit.ly/2FvMbpA
 */
const updateThatBeardYo = () => {
  client.get(
    "account/verify_credentials",
    { skip_status: true },
    (err, data) => {
      if (!err) {
        const { description } = data;
        updateBeardyBio(description);
      } else {
        console.log(`Beard fetch failed. -1 man points: ${err[0].message}`);
        process.exit(1);
      }
    }
  );
};

/*
 * Pull `Beardy Days` from prevuous bio and increment by 1
 */
const updateBeardyBio = previousBio => {
  const beardyDays =
    parseInt(
      previousBio.substring(
        previousBio.indexOf("*") + 1,
        previousBio.lastIndexOf("*")
      )
    ) + 1;

  const beardyBio = previousBio.replace(/\*(.*?)\*/i, `*${beardyDays}*`);
  const params = {
    description: beardyBio,
    skip_status: true
  };

  client.post("account/update_profile", params, function(err, data) {
    if (!err) {
      console.log("Beard updated. +1 man points");
    } else {
      console.log(`Beard update failed. -1 man points: ${err[0].message}`);
      process.exit(1);
    }
  });
};

updateThatBeardYo();
