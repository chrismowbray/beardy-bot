/* You will need a twitter develop account to generate the below keys

Example Keys:

  module.exports = {
    consumer_key: "your_twitter_api_key",
    consumer_secret: "your_twitter_api_secret_key",
    access_token_key: "your_twitter_access_token",
    access_token_secret: "your_twitter_access_token_secret"
  };
*/

module.exports = {
  consumer_key: process.env.K8S_SECRET_TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.K8S_SECRET_TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.K8S_SECRET_TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.K8S_SECRET_TWITTER_ACCESS_TOKEN_SECRET
};
